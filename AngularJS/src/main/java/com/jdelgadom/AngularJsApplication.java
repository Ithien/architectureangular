package com.jdelgadom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;

@SpringBootApplication
public class AngularJsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularJsApplication.class, args);
	}
}
