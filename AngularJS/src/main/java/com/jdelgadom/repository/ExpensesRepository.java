package com.jdelgadom.repository;

import com.jdelgadom.model.Expenses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jose on 12/10/2017.
 */
@Repository
public interface ExpensesRepository extends JpaRepository<Expenses, Serializable> {
  public Expenses findById(Integer id);
  public Expenses save(Expenses expenses);
}
