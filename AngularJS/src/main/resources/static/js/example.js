/**
 * Created by Jose on 08/10/2017.
 */
var myApp = angular.module('app',[]);

myApp.controller('HelloController', ['$scope', function($scope) {
    $scope.example = {text: 'This is AngularJS'};
}]);
