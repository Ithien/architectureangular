# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Angular Architecture Example with ThymeLeaf 
* 1.0.0
* https://bitbucket.org/Ithien/architectureangular/

### How do I get set up? ###

* mvn clean package
* Java 1.8
* pom dependencies
* h2 database example: http://localhost:8080/h2/login.do
* mvn clean package
* java -jar target/angularjs.jar

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact