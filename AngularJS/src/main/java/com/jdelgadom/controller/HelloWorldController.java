package com.jdelgadom.controller;

import com.jdelgadom.repository.ExpensesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Jose on 07/10/2017.
 */
@Controller
public class HelloWorldController {

  @Autowired
  ExpensesRepository expensesRepository;

  /**
   * @name helloWorld
   * @description Example to create a hello world.
   * @param  model object to comunicate with the view all the information need the view
   * @return ModelAndView for the "String" template selected, in the example "hello.html"
   * @datecreation 07/10/2017
   * @author Jose Delgado Moltó
   * @twitter @jdelgadomolto
   *
   */
  @RequestMapping(value="/hello", method = RequestMethod.GET)
  public ModelAndView helloWorld(Model model){
    model.addAttribute("name", "Sparta");
    return new ModelAndView("hello");
  }


  @RequestMapping(value="/helloAngular", method = RequestMethod.GET)
  public ModelAndView helloAngularJS(Model model){
    model.addAttribute("name", "Sparta");
    model.addAttribute("expenses", expensesRepository.findById(1));
    return new ModelAndView("angularexample");
  }

  @RequestMapping(value="/angularjs", method = RequestMethod.GET)
  public ModelAndView principal(Model model){
    return new ModelAndView("principal");
  }

}
